import png

def palette(c1, c2):
    return [(int(c1[0] + (c2[0] - c1[0]) * float(i) / 13),
             int(c1[1] + (c2[1] - c1[1]) * float(i) / 13),
             int(c1[2] + (c2[2] - c1[2]) * float(i) / 13)) for i in range(14)]

arancione = palette((0xe5, 0x98, 0x3a), (0xe1, 0x89, 0x34))
verde = palette((0x3d, 0x57, 0x31), (0x44, 0x61, 0x37))
arancione.append(arancione[-1])
arancione.append(arancione[-1])
verde.append(verde[-1])
verde.append(verde[-1])

for i in ["Cappellino", "Cappellone", "Fazzolettone", "Fiore", "Fuoco", "Pino", "Tenda"]:
    file_rosa = f"Rosa_{i}.png"
    file_verde = f"Verde_{i}.png"
    file_arancione = f"Arancione_{i}.png"
    r = png.Reader(file_rosa)
    width, height, rows, info = r.read()
    palette = r.palette()
    rows = list(rows)
    with open(file_rosa, "wb") as f:
         png.Writer(planes=1, width=width, height=height, palette=palette, bitdepth=4).write(f, rows)
    with open(file_verde, "wb") as f:
         png.Writer(planes=1, width=width, height=height, palette=verde, bitdepth=4).write(f, rows)
    with open(file_arancione, "wb") as f:
         png.Writer(planes=1, width=width, height=height, palette=arancione, bitdepth=4).write(f, rows)

