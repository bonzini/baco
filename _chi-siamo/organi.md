La Fondazione viene governata da un Consiglio di Amministrazione,
affiancato da un organo di indirizzo denominato Comitato dei Sostenitori.

Il primo Consiglio di Amministrazione, che dura in carica per tre anni, è così composto:

* Presidente: Andrea Germi

* Vice Presidente: Cristina Telazzi

* Consiglieri: Andrea Aresti, Matilde Conconi, Stefano Fiscato, Anna Mantegazza, Alberto Pizzi
