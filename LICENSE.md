---
permalink: /licenza
title: Licenza
---

Il contenuto di questo sito web è reso disponibile con una scelta tra queste
due licenze:

 * [GNU General Public License, version 2 or any later version](/license-gpl-2)
 * [Creative Commons Attribution-ShareAlike 4.0 International Public License](/license-cc-by-sa-4.0)
