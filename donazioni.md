---
title: Donazioni
permalink: /donazioni/
---

## Sostieni il nostro progetto

Insieme, con il tuo contributo, possiamo rendere sempre più concreto il nostro progetto.

Puoi sostenere la nostra attività con una donazione spontanea [on line](https://buonacausa.org/cause/base-scout-saronno), o con bonifico bancario intestato a Fondazione Ba.Co.: IBAN <strong class="iban"><span>IT</span><span>54</span><span>F</span><span>06230</span><span>50520</span><span>0000</span><span>1523</span><span>8238</span></strong>. Consigliamo di inserire nella causale la dicitura &ldquo;erogazione liberale&rdquo;.

Aiutaci a portare avanti le nostra sfida per realizzare uno spazio per crescere, a disposizione di bambini, giovani e ragazzi. Puoi fare la tua parte, piccola o grande che sia, donando liberamente alla Fondazione, con la garanzia che il 100% di ogni donazione viene destinato al progetto.

Attraverso il tuo sostegno, possiamo rendere ogni giorno più concreto l’obiettivo di trasformare il nostro spazio per crescere in uno spazio condiviso e accessibile a tutti!

## Agevolazioni fiscali

Per le persone fisiche, le erogazioni fiscali alla Fondazione
Ba.Co. ETS possono fruire ai sensi dell’art.&nbsp;83,
D.Lgs.&nbsp;n.&nbsp;117/2017:
* della detrazione IRPEF del 30%, nel limite di spesa annuale di Euro 30.000;
* alternativamente, della deduzione nel limite del 10% del reddito complessivo dichiarato dal soggetto erogatore.

Per le società o gli enti, le liberalità in denaro o in natura erogate da enti e società a favore degli Enti del Terzo Settore sono deducibili nel limite del 10% del reddito complessivo.

Per fruire della detrazione o della deduzione <strong>l’erogazione liberale in denaro deve essere stata effettuata tramite banca, posta o sistemi di pagamento “tracciabili”</strong> (bollettino, bonifico, bancomat, carte di credito, etc.).

Non sono quindi detraibili o deducibili le erogazioni liberali effettuate in contanti.  Per richiedere la ricevuta della erogazione liberale è necessario comunicare <strong>nome, codice fiscale e indirizzo</strong> al nostro indirizzo mail <a href="mailto:info@basecolombarasaronno.org">info@basecolombarasaronno.org</a>.

<section markdown="1">

## 5 per mille

Per destinare il 5 per mille a Ba.Co. basta compilare l'apposito spazio
della dichiarazione dei redditi:

* Firma nel primo riquadro, dedicato al "Sostegno degli Enti del Terzo settore iscritti nel RUNTS".

* Inserisci il codice fiscale di Ba.Co. <strong>94032260120</strong>

Se presenti il 730 precompilato, seleziona &ldquo;Sostegno degli Enti
del Terzo settore iscritti nel RUNTS&rdquo; nella sezione dedicata al
5 per mille e inserisci il codice fiscale.

</section>

{% include evidenzia-sezione.html %}
