---
layout: post
data_evento: 2023-12-08
title:  "Villaggio di Natale"
categories: ['raccolta fondi', 'caronno pertusella']
---

**Venerdì 8 dicembre** saremo presenti presso il
**Villaggio di Natale** a Caronno Pertusella, in
[via Adua 119](https://www.google.com/maps/place/Via+Adua,+119,+21042+Caronno+Pertusella+VA/@45.6539551,8.9996592,14z/data=!4m6!3m5!1s0x47869476847916df:0x3f9d36c5209f30ba!8m2!3d45.5977451!4d9.0434311!16s%2Fg%2F11bw43cnr8?entry=ttu).

Vi aspettiamo con lo zucchero filato, i nostri gadget, e l'autofinanziamento
del reparto Ohio (scout Saronno 1).

<img src="{% attachment locandina.jpg %}">
