---
layout: post
data_evento: 2023-09-23
title:  "Cena delle genti"
categories: ['raccolta fondi', cena, baranzate]
---

**Sabato 23 settembre** alle ore 19:30, presso la
[Baita degli Alpini](https://www.google.com/maps/place/Baita+degli+Alpini/@45.5989342,9.0399898,18.75z/data=!4m15!1m8!3m7!1s0x4786938a0f7ddb19:0xaa509edcaf9bed8e!2sVia+Pola,+21042+Caronno+Pertusella+VA!3b1!8m2!3d45.599001!4d9.0400568!16s%2Fg%2F119vvq_zb!3m5!1s0x4786954d3813c2c3:0xd61b0002f2a2bd5a!8m2!3d45.5989038!4d9.041794!16s%2Fg%2F11fw3ftc25?entry=ttu)
a Caronno Pertusella, il gruppo scout Baranzate 1 organizza la
[Cena delle Genti]({% attachment locandina.pdf %}).
Il ricavato dell’iniziativa sarà devoluto al progetto Ba.Co.

La prenotazione è obbligatoria fino ad esaurimento dei 50 posti
disponibili; il gruppo richiede ai partecipanti un'offerta minima
di 28€.

Per riservare un posto, contattare il gruppo tramite WhatsApp al numero
[335&nbsp;1305563](https://wa.me/%2B393351305563/).
