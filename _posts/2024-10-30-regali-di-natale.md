---
layout: post
title:  "Birre e marmellate per Natale!"
categories: ['raccolta fondi', birra, confetture, 'refettorio ambrosiano', 'caritas ambrosiana']
---

Anche quest'anno, dopo il successo del 2023, Ba.Co. organizza
una raccolta fondi natalizia!

Oltre al secondo appuntamento con le [<strong>Ba.Co. birre</strong>](../../../../2023/10/10/baco-birra/)
artigianali, la novità di quest'anno sono le
<strong>Ba.Co. confetture</strong>, prodotte in collaborazione con il
[Refettorio Ambrosiano](https://refettorioambrosiano.it/) e la [Caritas
Ambrosiana](https://caritasambrosiana.it).

Per le birre chiediamo un'offerta minima è di 12€ per ogni confezione
da tre bottiglie da 33&nbsp;cl (pop ale, IPA, honey bock).

Le confezioni da tre vasetti da 200&nbsp;g (pesche, pere e vaniglia, mele e
cannella) invece prevedono un'offerta minima di 15€.

È possibile prenotare fin da ora una o più confezioni [via Whatsapp](https://wa.me/%2B3500730077/)
o con una mail a [info@basecolombarasaronno.org](mailto:info@basecolombarasaronno.org),


<img src="{% attachment locandina.jpg %}">
