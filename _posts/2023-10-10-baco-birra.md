---
layout: post
title:  "È arrivata la Ba.Co. birra!"
categories: ['raccolta fondi', birra, regali]
---

Siamo orgogliosi di presentare le tre <strong>Ba.Co. birre</strong>!
&ldquo;Acero&rdquo;, &ldquo;Castagno&rdquo; e &ldquo;Quercia&rdquo;
sono tre birre artigianali prodotte dal [Microbirrificio
DOPPIOBAFFO](https://doppiobaffo.com/) di Chignolo Po (PV):

* Acero è una IPA ambrata, dall'aroma fruttato e dal gusto caramellato;
ottima per accompagnare piatti dai gusti decisi

* Castagno è una birra bock (a bassa fermentazione) aromatizzata con miele di castagno,
dolce e corposa

* Quercia è una birra leggera, poco amara e spiccatamente agrumata, ottima
con piatti leggeri di ogni tipo

Le birre saranno disponibili da metà novembre, ma è possibile prenotare fin
da ora una o più confezioni da tre bottiglie da 33&nbsp;cl con un'offerta
minima di 12€ per ogni confezione. Potete prenotare
[via Whatsapp](https://wa.me/%2B3500730077/), con una mail a
[info@basecolombarasaronno.org](mailto:info@basecolombarasaronno.org),
alle feste di apertura dell'anno scout dei nostri
[gruppi fondatori](https://www.basecolombarasaronno.org/chi-siamo/#gruppi),
e ai nostri prossimi eventi di autofinanziamento.

<div class="row flush">
	<img style="display: block; float: left; width: max(25%, 250px); object-fit: contain"
		src="{% attachment acero.jpg %}"
		title="Etichetta birra &ldquo;Acero&rdquo;"
		alt="Etichetta birra &ldquo;Acero&rdquo;">
	<img style="display: block; float: left; width: max(25%, 250px); object-fit: contain"
		src="{% attachment castagno.jpg %}"
		title="Etichetta birra &ldquo;Castagno&rdquo;"
		alt="Etichetta birra &ldquo;Castagno&rdquo;">
	<img style="display: block; float: left; width: max(25%, 250px); object-fit: contain"
		src="{% attachment quercia.jpg %}"
		title="Etichetta birra &ldquo;Quercia&rdquo;"
		alt="Etichetta birra &ldquo;Quercia&rdquo;">
</div>
