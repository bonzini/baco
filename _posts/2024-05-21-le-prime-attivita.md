---
layout: post
title:  "Le prime attività scout"
categories: ["attività", "attività scout"]
colorbox: true
---

Come anticipato nella [newsletter di marzo 2024](../../../03/30/newsletter/),
nelle scorse settimane si sono tenuti presso Ba.Co. i due campi di
San Giorgio delle zone AGESCI Promise e Brianza/Milano Nord.  I
reparti delle due zone hanno trascorso due giorni alla Cascina
Colombara e fatto servizio per la base.

Abbiamo anche ospitato la prima cerimonia della Partenza, la
conclusione del cammino educativo nell'ambito dello scautismo,
e un'impresa di squadriglia con le Volpi del gruppo Magenta 1.
La squadriglia ha pernottato alla base sabato e domenica scorsi
ed è stata di grande aiuto!


<div class="row">
<div class="5u">
<a title="indicazioni predisposte per la zona Brianza/Milano Nord" href="{% attachment brimino-2.jpg %}" class="colorbox"><img
  src="{% attachment brimino-2.jpg %}"></a>
</div><div class="only-mobile">
<a title="La prima partenza" href="{% attachment partenza-large.jpg %}" class="colorbox"><picture style="object-fit: contain;"><source
  media="(min-resolution: 130dpi)" srcset="{% attachment partenza-large.jpg %}"><img
  src="{% attachment partenza-small.jpg %}"></picture></a>
<a title="S. Giorgio zona Brianza/Milano Nord" href="{% attachment brimino-1.jpg %}" class="colorbox"><img
  src="{% attachment brimino-1.jpg %}"></a>
<a title="S. Giorgio zona Brianza/Milano Nord" href="{% attachment brimino-3-large.jpg %}" class="colorbox"><picture style="object-fit: contain;"><source
  media="(min-resolution: 130dpi)" srcset="{% attachment brimino-3-large.jpg %}"><img
  src="{% attachment brimino-3-small.jpg %}"></picture></a>
<a title="S. Giorgio zona Promise" href="{% attachment promise-1-large.jpg %}" class="colorbox"><picture style="object-fit: contain;"><source
  media="(min-resolution: 130dpi)" srcset="{% attachment promise-1-large.jpg %}"><img
  src="{% attachment promise-1-small.jpg %}"></picture></a>
<a title="S. Giorgio zona Promise" href="{% attachment promise-2.jpg %}" class="colorbox"><img
  src="{% attachment promise-2.jpg %}"></a>
<a title="S. Giorgio zona Promise" href="{% attachment promise-3.jpg %}" class="colorbox"><img
  src="{% attachment promise-3.jpg %}"></a>
</div>

