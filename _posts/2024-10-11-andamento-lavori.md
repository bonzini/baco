---
layout: post
title:  "Aggiornamento ottobre 2024"
categories: [newsletter, progetti, lavori]
colorbox: true
---

Anche se la scorsa primavera si sono svolte le prime attività presso
Ba.Co., erano ancora molti i lavori da fare, e durante l'estate i
progressi sono stati notevoli!

## Lavori

Abbiamo completato la realizzazione di 700&nbsp;m di recinzione
e del cancello di ingresso, e l'allacciamento idrico.

Per quanto riguarda la vegetazione, abbiamo seminato l'erba per
contrastare la crescita dei rovi e contiamo di iniziare presto con la
piantumazione.  Occorrerà piantare centinaia di alberi e arbusti per
risanere le ferite lasciate dall'incendio dell'estate 2022.

<div class="row">
<div class="5u"><a href="{% attachment cancello1-large.jpg %}" class="colorbox"><picture style="object-fit: contain;"><source
  media="(min-resolution: 130dpi)" srcset="{% attachment cancello1-large.jpg %}"><img
  src="{% attachment cancello1-small.jpg %}"></picture></a>
</div><div class="only-mobile">
<a href="{% attachment cancello2-large.jpg %}" class="colorbox"><picture><source
  media="(min-resolution: 130dpi)" srcset="{% attachment cancello2-large.jpg %}"><img
  src="{% attachment cancello2-small.jpg %}"></picture></a>
<a href="{% attachment recinzione-large.jpg %}" class="colorbox"><picture><source
  media="(min-resolution: 130dpi)" srcset="{% attachment recinzione-large.jpg %}"><img
  src="{% attachment recinzione-small.jpg %}"></picture></a>
<a href="{% attachment acqua1-large.jpg %}" class="colorbox"><picture><source
  media="(min-resolution: 130dpi)" srcset="{% attachment acqua1-large.jpg %}"><img
  src="{% attachment acqua1-small.jpg %}"></picture></a>
<a href="{% attachment acqua2-large.jpg %}" class="colorbox"><picture><source
  media="(min-resolution: 130dpi)" srcset="{% attachment acqua2-large.jpg %}"><img
  src="{% attachment acqua2-small.jpg %}"></picture></a>
<a href="{% attachment prato-large.jpg %}" class="colorbox"><picture><source
  media="(min-resolution: 130dpi)" srcset="{% attachment prato-large.jpg %}"><img
  src="{% attachment prato-small.jpg %}"></picture></a>
</a>
</div>
</div>

## Raccolta fondi

Siamo stati presenti alla Strasaronno e il gruppo Saronno&nbsp;1 ha
presentato il progetto nel corso della manifestazione "Idee in
corso" del 6 ottobre scorso.

Inoltre, dopo il successo della Ba.Co. birra dello scorso anno, a
breve lanceremo il secondo appuntamento con la raccolta fondi
natalizia.

<div class="row">
<div class="5u"><a href="{% attachment strasaronno-large.jpg %}" class="colorbox"><picture style="object-fit: contain;"><source
  media="(min-resolution: 130dpi)" srcset="{% attachment strasaronno-large.jpg %}"><img
  src="{% attachment strasaronno-small.jpg %}"></picture></a>
</div><div class="only-mobile">
<a href="{% attachment scout1-large.jpg %}" class="colorbox"><picture><source
  media="(min-resolution: 130dpi)" srcset="{% attachment scout1-large.jpg %}"><img
  src="{% attachment scout1-small.jpg %}"></picture></a>
<a href="{% attachment scout2-large.jpg %}" class="colorbox"><picture><source
  media="(min-resolution: 130dpi)" srcset="{% attachment scout2-large.jpg %}"><img
  src="{% attachment scout2-small.jpg %}"></picture></a>
</a>
</div>
</div>
