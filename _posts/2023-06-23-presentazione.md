---
layout: post
title:  "Presentato alla cittadinanza il progetto Ba.Co."
categories: ['presentazione', 'raccolta fondi', donazioni]
---

Ieri sera, nell’ambito del progetto “Partecipiamo”, due delegati della
fondazione Ba.Co. ETS hanno presentato il progetto di creare una base
scout nei pressi della Cascina Colombara.

I 30.000 metri quadri di terreno, situati in via Vicinale, sono
ideali per permettere ai ragazzi di vivere la natura.  Una volta
recintato, la base potrà essere non solo un punto di riferimento
per i gruppi scout lombardi, ma anche un'area attrezzata a disposizione
delle scolaresche e una ricchezza per l'intera città.

Il progetto, hanno spiegato i delegati, darà vita e movimento al quartiere,
trasformando un’area abbandonata in una risorsa.

Contemporaneamente è stata presentata la piattaforma per la raccolta
fondi online, attiva su <a href="https://buonacausa.org/cause/base-scout-saronno">buonacausa.org</a> o <a href="https://www.paypal.com/donate/?hosted_button_id=GBP5JKJKJ5QZA">PayPal</a>.
