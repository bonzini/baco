---
layout: post
title:  "Ba.Co. alla festa del Seminario"
categories: ['raccolta fondi', 'venegono inferiore']
colorbox: true
---

Lo scorso **1 maggio** Ba.Co. era presente con il suo banchetto
a Venegono Inferiore per la Festa del Seminario 2024.
Abbiamo presentato il nostro progetto e venduto, assieme
ai gadget, marmellate preparate per l'occasione dal gruppo scout
Saronno&nbsp;1!  Presente anche l'arcivescovo Mons.&nbsp;Mario Delpini.

Il ricavato è stato devoluto in parte a Ba.Co. e in parte alla
comunità capi di Saronno, per finanziarne la partecipazione
alla [Route Nazionale Capi 2024](https://rn24.agesci.it/).

<div class="row">
<div class="5u"><a href="{% attachment arcivescovo-large.jpg %}" class="colorbox"><picture style="object-fit: contain;"><source
  media="(min-resolution: 130dpi)" srcset="{% attachment arcivescovo-large.jpg %}"><img
  src="{% attachment arcivescovo-small.jpg %}"></picture></a>
</div><div class="only-mobile">
<a href="{% attachment banchetto-large.jpg %}" class="colorbox"><picture><source
  media="(min-resolution: 130dpi)" srcset="{% attachment banchetto-large.jpg %}"><img
  src="{% attachment banchetto-small.jpg %}"></picture></a>
<a href="{% attachment gruppo-large.jpg %}" class="colorbox"><picture><source
  media="(min-resolution: 130dpi)" srcset="{% attachment gruppo-large.jpg %}"><img
  src="{% attachment gruppo-small.jpg %}"></picture></a>
<a href="{% attachment gadget-large.jpg %}" class="colorbox"><picture><source
  media="(min-resolution: 130dpi)" srcset="{% attachment gadget-large.jpg %}"><img
  src="{% attachment gadget-small.jpg %}"></picture></a>
<a href="{% attachment locandina-large.jpg %}" class="colorbox"><picture><source
  media="(min-resolution: 130dpi)" srcset="{% attachment locandina-large.jpg %}"><img
  src="{% attachment locandina-small.jpg %}"></picture></a>
</a>
</div>

