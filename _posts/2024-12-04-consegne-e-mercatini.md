---
layout: post
data_evento: 2024-12-15
title:  "Prossimi ritiri birre e marmellate"
categories: ['raccolta fondi', 'caronno pertusella', 'saronno']
lancio: |
  domenica 15 dicembre presso i mercatini di via Roma a Cesate
---

Chi ha già ordinato <a href="../../10/30/regali-di-natale/">le nostre confezioni di marmellata e di birra</a>
potrà già ritirarle nelle seguenti occasioni:

* <strong>venerdì 6 dicembre</strong> a partire dalle ore 21 presso
la [Baita degli Alpini](https://www.google.com/maps/place/Baita+degli+Alpini/@45.5989342,9.0399898,18.75z/data=!4m15!1m8!3m7!1s0x4786938a0f7ddb19:0xaa509edcaf9bed8e!2sVia+Pola,+21042+Caronno+Pertusella+VA!3b1!8m2!3d45.599001!4d9.0400568!16s%2Fg%2F119vvq_zb!3m5!1s0x4786954d3813c2c3:0xd61b0002f2a2bd5a!8m2!3d45.5989038!4d9.041794!16s%2Fg%2F11fw3ftc25?entry=ttu) di Caronno Pertusella.

* <strong>sabato 7</strong> nel pomeriggio e <strong>domenica 8</strong>
al nostro stand presso i mercatini di Natale di Saronno

* <strong>domenica 8 dicembre</strong> al
**villaggio di Natale** di Caronno Pertusella, in
[via Adua 119](https://www.google.com/maps/place/Via+Adua,+119,+21042+Caronno+Pertusella+VA/@45.6539551,8.9996592,14z/data=!4m6!3m5!1s0x47869476847916df:0x3f9d36c5209f30ba!8m2!3d45.5977451!4d9.0434311!16s%2Fg%2F11bw43cnr8?entry=ttu).

* <strong>domenica 15 dicembre</strong> ai mercatini di Natale
di Cesate.

Vi aspettiamo!

<img src="{% attachment locandina.jpg %}">
