---
layout: post
data_evento: 2024-01-28
title:  "Pomeriggio danzante &mdash; balli folk"
categories: ['raccolta fondi', musica]
---

**Domenica 28 gennaio** alle ore 17:00, presso il PalaEXBO di Saronon
(via Piave 1), ospiteremo un pomeriggio di balli popolari.  I musicisti
del [Collettivo musicale ReUm](https://www.facebook.com/ReUmCollettivo)
suoneranno musiche della tradizione dei balli popolari, e a condurre
le danze sarà Milena Blasevich.

La prenotazione via Whatsapp al numero
[350&nbsp;0730077](https://wa.me/%2B3500730077/) è obbligatoria fino
ad esaurimento dei posti disponibili; è richiesta un'offerta minima di
13€ a persona.

L'intero ricavato dell'iniziativa sarà devoluto al progetto Ba.Co.

<img src="{% attachment locandina.jpg %}">
