---
layout: post
title:  "Osservazione del cielo presso Ba.Co."
categories: ["attività"]
---

Venerdì 11 ottobre (settimana prossima) il [Gruppo Astrofili di Saronno](https://www.astrofilisaronno.it)
proporrà una serata di osservazione del cielo presso la base scout.  La serata è aperta
a tutti gli interessati; è la prima attività esterna allo scoutismo che viene tenuta presso Ba.Co.!

L'appuntamento è per le ore 20. Ovviamente, l'evento si terrà solo in caso di tempo favorevole, altrimenti sarà rimandato.

Per iscrivervi compilate l'apposito [modulo](https://forms.gle/uhv3VV3tfqvAzfEs5).

<a href="{% attachment locandina.pdf %}" class="colorbox"><img
  src="{% attachment locandina.png %}"></a>

Proprio in questi giorni è possibile osservare la cometa C/2023 A3 Tsuchinshan-Atlasche che, dopo essere passata a circa 60 milioni di chilometri dal Sole, ha cominciato il viaggio che la porterà vicino alla Terra. La cometa sarà visibile al tramonto e raggiungerà il picco di luminosità il 9 ottobre. Pochi giorni dopo, il 12 ottobre, raggiungerà il perigeo con un passaggio ravvicinato a circa 71 milioni di chilometri dalla Terra.

Per sapere cos'altro osserveremo l'11 ottobre potete scaricare l'app Stellarium sul vostro smartphone.

<a href="https://play.google.com/store/apps/details?id=com.noctuasoftware.stellarium_free&hl=it"><img
  src="/assets/images/googleplay-it.png" height="70" style="margin-right: 1em;"></a> <a
  href="https://apps.apple.com/us/app/stellarium-mobile-star-map/id1458716890"><img
  src="/assets/images/appstore-it.svg" height="70"></a>
