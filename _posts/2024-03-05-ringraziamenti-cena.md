---
layout: post
data_evento: 2024-03-02
title:  "Ringraziamenti e ovetti di Pasqua"
categories: ['raccolta fondi', cena, pasqua]
colorbox: true
---

Ringraziamo la parrocchia di Cesate, gli scout di ASSIM e tutti
i partecipanti alla cena <q><strong>Ba.Co &amp; I Sapori del
Marocco</strong></q>, che si è tenuta lo scorso sabato 2 marzo presso
l'oratorio di Cesate.

Ricordiamo che l'utile è stato devoluto interamente al progetto Ba.Co.

Durante la cena abbiamo anche proposto la vendita di ovetti di cioccolato
ripieni per Pasqua. Per ogni confezione da 200 g chiediamo un'offerta
minima di 7€.  Per prenotare una o più confezioni, contattateci tramite
WhatsApp al numero [350&nbsp;0730077](https://wa.me/%2B393500730077/).

<div class="row">
<div class="5u"><a href="{% attachment tajine-large.jpg %}" class="colorbox"><picture style="object-fit: contain;"><source
  media="(min-resolution: 130dpi)" srcset="{% attachment tajine-large.jpg %}"><img
  src="{% attachment tajine-small.jpg %}"></picture></a>
</div><div class="only-mobile">
<a href="{% attachment fagiolata-large.jpg %}" class="colorbox"><picture><source
  media="(min-resolution: 130dpi)" srcset="{% attachment fagiolata-large.jpg %}"><img
  src="{% attachment fagiolata-small.jpg %}"></picture></a>
<a href="{% attachment hummus-large.jpg %}" class="colorbox"><picture><source
  media="(min-resolution: 130dpi)" srcset="{% attachment hummus-large.jpg %}"><img
  src="{% attachment hummus-small.jpg %}"></picture></a>
<a href="{% attachment dolci-large.jpg %}" class="colorbox"><picture><source
  media="(min-resolution: 130dpi)" srcset="{% attachment dolci-large.jpg %}"><img
  src="{% attachment dolci-small.jpg %}"></picture></a>
<a href="{% attachment tavoli.jpg %}" class="colorbox">
  <img src="{% attachment tavoli.jpg %}">
</a>
</div>
<div class="6u"><a href="{% attachment locandina-ovetti.jpg %}" class="colorbox">
  <img src="{% attachment locandina-ovetti.jpg %}" style="object-fit: contain;">
</a></div>

