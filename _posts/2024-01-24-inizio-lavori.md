---
layout: post
title:  "Iniziati i lavori alla Cascina Colombara"
categories: ['lavori in corso']
colorbox: true
---

A due mesi dall'acquisto del terreno, ieri, martedì 23 gennaio, sono
iniziati i lavori alla Cascina Colombara dove sorgerà [la nostra base
scout](/progetto)!

Durante l'incendio che ha colpito la zona nel 2022, più di 800 alberi
erano andati distrutti. I primi lavori da svolgere sono tesi a liberare
l'area, per poterla ripiantumare. Nei prossimi mesi potremo così
ricostituire un parco funzionale al nostro progetto.

<a href="{% attachment foto3.jpg %}" class="colorbox">
 <img src="{% attachment foto3.jpg %}" style="max-width: 80vw; max-height: 80vh">
</a>
<div style="display: none;">
<a href="{% attachment foto1.jpg %}" class="colorbox">
 <img src="{% attachment foto1.jpg %}">
</a>
<a href="{% attachment foto2.jpg %}" class="colorbox">
 <img src="{% attachment foto2.jpg %}">
</a>
</div>
