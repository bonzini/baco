---
layout: post
data_evento: 2023-11-04
title:  "Apericena in musica"
categories: ['raccolta fondi', musica, teatro]
---

**Sabato 4 novembre** alle ore 19:30, presso l'oratorio di
via Legnani a Saronno, ospiteremo lo spettacolo musicale
<q>Dietro a frasi di canzoni</q> della band <q>Quelli che</q>.
Lo spettacolo presenterà i temi dell'amore, del destino e della guerra
come appaiono nelle canzoni di Fabrizio de Andrè.

Precederà il concerto un apericena con piatti freddi e caldi;
il ricavato dell’iniziativa sarà devoluto al progetto Ba.Co.

La prenotazione via Whatsapp al numero
[350&nbsp;0730077](https://wa.me/%2B3500730077/) è obbligatoria fino
ad esaurimento dei posti disponibili; è richiesta un'offerta minima di
18€ a persona.

<img src="{% attachment locandina.jpg %}">
