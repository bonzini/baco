---
layout: post
data_evento: 2024-03-02
title:  "Aggiornamento marzo 2024"
categories: [newsletter, progetti, "attività", "attività scout"]
colorbox: true
---

Molte le novità durante i mesi di febbraio e marzo!
Il nostro progetto è su un’ottima strada e prevediamo
in tempi brevi le prime attività presso la base.

## Pulizia dell'area

In febbraio e in marzo si sono svolte alcune giornate di pulizia
dell’area.  Per due weekend abbiamo chiamato a raccolta genitori, scout,
amici, conoscenti per la pulizia del terreno.  Anche Lega Ambiente Saronno
è venuta ad aiutarci. Abbiamo eliminato la maggior parte dei rifiuti, ma
purtroppo ancora oggi si trovano rifiuti più piccoli sparsi sul terreno.
Scavando per livellare qualche area se ne trovano altri sotterrati.
Tanto è ancora da fare! Le prossime giornate di pulizia e lavori alla
base sono indette per il weekend dopo Pasqua, ovvero **sabato 6 e domenica
7 aprile**.

## Alberature

Tutti gli alberi morti sono stati abbattuti, il legname delle
latifoglie è già stato portato via mentre parte dei pini strobi
è ancora sul campo.

Ora la zona sembra ancora più vasta, con la costruzione ferita dal fuoco
ben evidente, e sembra davvero solo un campo.  Solo una ventina di querce
rosse sono rimaste in piedi e speriamo che riescano a rimettersi dalle
bruciature subite.

I prossimi passi sono la fresatura della terra per togliere le radici
superficiali delle piante, la semina dell'erba per contrastare la
crescita dei rovi, e la piantumazione con essenze tipiche del
nostro territorio.

## Altri progetti

Abbiamo elaborato il progetto per la realizzazione di una recinzione,
la cui realizzazione è già iniziata lungo il lato nord-ovest,
e di un cancello di ingresso. Anche i lavori sulla recinzione proseguiranno
sabato 6 e domenica 7 aprile.

Altri importanti lavori in programma comprendono l'allacciamento idrico
e il recupero dei due stabili, anch'essi feriti dall'incendio di luglio
2022.

## Raccolta fondi

Per chi volesse sostenere il progetto Ba.Co. ricordiamo la possibilità
di [donare il 5x1000]({{ relative_root }}/donazioni#per-mille), oltre
all'appuntamento di **sabato 20 aprile** con il musical
[&ldquo;Oh Mamma! è tutto molto greco&rdquo;](../../09/musical/)!
Per riservare un posto, contattateci tramite WhatsApp al numero
[350&nbsp;0730077](https://wa.me/%2B393500730077/).

## I primi eventi scout!

Il prossimo 20-21 aprile e 4-5 maggio la base ospiterà i due campi di
San Giorgio delle zone AGESCI Promise e Brianza/Milano Nord.  Siamo molto
contenti di questa scelta, grazie alla quale più di 1000 ragazzi dei
reparti delle due zone potranno apprezzare il nostro lavoro e fare
servizio per la base stessa!
