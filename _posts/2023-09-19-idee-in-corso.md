---
layout: post
data_evento: 2023-10-01
title:  "Idee in corso"
categories: ['raccolta fondi', saronno, scout]
---

**Domenica 1 ottobre**, dalle 10 alle 18, Ba.Co. e il gruppo scout
AGESCI Saronno 1 parteciperanno a <strong>Idee in corso</strong>,
una “vetrina” organizzata dal comune di Saronno per promuovere le
proposte socio-culturali e per il tempo libero presenti in città.

Presso lo stand di Ba.Co. sarà presente un mercatino il cui ricavato
finanzierà l'acquisto e la sistemazione della futura base; sarà inoltre
possibile ritirare materiale informativo sulla nostra iniziativa.

Dalle 15 alle 18 gli scout del gruppo Saronno 1 proporranno dei
laboratori.
