---
layout: post
data_evento: 2024-03-02
title:  "Cena &ldquo;Ba.Co & I Sapori del Marocco&rdquo;"
categories: ['raccolta fondi', cena, assim]
---

**Sabato 2 marzo** alle ore 19:30, presso
l'[Oratorio San Francesco di Cesate](https://maps.app.goo.gl/kDBuLPGxxtm36gATA)
(via Concordia 6) si terrà la cena <q><strong>Ba.Co &amp; I Sapori del
Marocco</strong></q>.  Il ricavato dell’iniziativa, organizzata in
collaborazione con gli scout di ASSIM, sarà devoluto al progetto Ba.Co.

La prenotazione è obbligatoria fino ad esaurimento dei posti
disponibili; viene richiesta ai partecipanti un'offerta minima
di 25€ per gli adulti e 12€ per i bambini sotto i 10&nbsp;anni.

Per riservare un posto, contattateci tramite WhatsApp al numero
[350&nbsp;0730077](https://wa.me/%2B393500730077/).

<img src="{% attachment locandina.jpg %}">

