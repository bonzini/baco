---
layout: post
title:  "🍺 Ritiro delle birre prenotate"
data_evento: 2023-12-16
categories: ['raccolta fondi', birra, regali, donazioni]
lancio: |
  Sabato 16 dicembre dalle 15 alle 19 presso [Baita degli Alpini](https://www.google.com/maps/place/Baita+degli+Alpini/@45.5989342,9.0399898,18.75z/data=!4m15!1m8!3m7!1s0x4786938a0f7ddb19:0xaa509edcaf9bed8e!2sVia+Pola,+21042+Caronno+Pertusella+VA!3b1!8m2!3d45.599001!4d9.0400568!16s%2Fg%2F119vvq_zb!3m5!1s0x4786954d3813c2c3:0xd61b0002f2a2bd5a!8m2!3d45.5989038!4d9.041794!16s%2Fg%2F11fw3ftc25?entry=ttu)
---

Chi non ha ancora ritirato le <a href="../../../10/10/baco-birra/">birre</a>
prenotate potrà farlo presso
la [Baita degli Alpini](https://www.google.com/maps/place/Baita+degli+Alpini/@45.5989342,9.0399898,18.75z/data=!4m15!1m8!3m7!1s0x4786938a0f7ddb19:0xaa509edcaf9bed8e!2sVia+Pola,+21042+Caronno+Pertusella+VA!3b1!8m2!3d45.599001!4d9.0400568!16s%2Fg%2F119vvq_zb!3m5!1s0x4786954d3813c2c3:0xd61b0002f2a2bd5a!8m2!3d45.5989038!4d9.041794!16s%2Fg%2F11fw3ftc25?entry=ttu) di Caronno Pertusella
nelle seguenti date:

* <strong>venerdì 1 dicembre</strong> a partire dalle ore 21

* <strong>sabato 16 dicembre</strong> dalle 15 alle 19

Ricordiamo a coloro che volessero godere delle detrazioni fiscali che è
necessario utilizzare metodi di pagamento tracciabili. Consigliamo pertanto
di effettuare una donazione tramite il portale
<a href="https://buonacausa.org/cause/base-scout-saronno">BuonaCausa</a>
e di inserire i vostri dati (<strong>nome, codice fiscale e indirizzo</strong>)
come &ldquo;messaggio privato al beneficiario&rdquo;. Sarà sufficiente comunicare
l'identificativo della donazione quando ritirerete le confezioni.

Potete trovare maggiori informazioni sulle detrazioni fiscali sulla
pagina dedicata alle <a href="{{ relative_root }}/donazioni">donazioni</a>.

<div class="7u -1u flush">
	<div style="overflow:hidden;width:100%;height:500px;"><div
		id="embed-map-display"
		style="height:100%; width:100%;max-width:100%;"><iframe
			style="height:100%;width:100%;border:0;"
			frameborder="0"
			src="https://www.google.com/maps/embed/v1/place?zoom=16&center=45.5989342,9.0399898&key=AIzaSyBFw0Qbyq9zTFTd-tUY6dZWTgaQzuU17R8&q=Baita+degli+Alpini+Caronno"></iframe>
		</div>
	</div>
</div>
