---
layout: post
data_evento: 2023-09-09
title:  "Cena e caccia al tesoro al Parco delle Groane"
categories: ['raccolta fondi', cena, assim, 'caccia al tesoro']
---

**Sabato 9 settembre** verrà presentato il progetto Ba.Co. presso la sede del Parco delle Groane in via Polveriera 2 a Solaro, nell'ambito dell'incontro degli storici e dei collezionisti scout di tutta Italia. Alle ore 20 gli scout di Assim invitano tutti a partecipare alla **cena marocchina** all’aperto. La quota di partecipazione è di €18 ed è richiesta la prenotazione a [segreteria@aicos-italia.it](mailto:segreteria@aicos-italia.it).

Alle ore 8.30 di domenica 10 settembre AICoS, l'Associazione Italiana Collezionisti Scout, organizza la **grande caccia al tesoro scout**, aperta a bambini, ragazzi e adulti, scout e non. È richiesta iscrizione per [mail ad AICoS](mailto:segreteria@aicos-italia.it) entro il 6 settembre. La quota di partecipazione di €1 sarà devoluta al progetto Base Scout Colombara.

Per maggiori informazioni, la [locandina](https://www.facebook.com/groups/32742198499/permalink/10160706039143500) è disponibile su Facebook.
